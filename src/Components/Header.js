import React, { Component } from 'react';
import logo from '../assets/logo1.png';
import Search from './Search.js';


class Header extends Component {
  render() {
    return (
      <div className="App-Header">
        <div className="header-logo"><img src={logo} alt="Shpock logo"/></div>
        <Search updateJson={this.props.updateJson}/>
      </div>
    );
  }
}

export default Header;
