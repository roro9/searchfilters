import React, { Component } from 'react';
import Filters from './Filters.js';

const defaultState = {
  searchText: 'Search...',
  filtersVisible: false,
  filter_days: 10,
  filter_sortBy: 'Distance',
  filter_radius: 25,
  filter_category: 'All',
  filter_min_price: 1,
  filter_max_price: 1000
}

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = { ...defaultState}
  }

  componentDidMount() {
    // if clicked out of search-box..close filters overlay
    window.addEventListener('click', e => {   
      if (!document.getElementsByClassName('header-search')[0].contains(e.target)){
        if(this.state.filtersVisible) {
          console.log('clicked outside search, closing filters');
          this.setState({filtersVisible: false});
        }
      }
    });
  }

  /**
   * update state on changing values in filters
   */
  handleChangeFor = (prop, value) => {
    this.setState({
      [prop]: value
    });
  }

  handleSearchBoxChange = (e) => {
    this.setState({ 
      searchText: e.target.value,
      filtersVisible: e.target.value ? true : false 
    });
  }

  /**
   * On clicking Search Input box...if default text..clear it
   * or if other text..open the filters overlay
   */
  onSearchBoxClick = (e) => {
    if(this.state.searchText === 'Search...') {
      this.setState({searchText: ''});
    } else if (this.state.searchText) {
      this.setState({filtersVisible: true});
    }
  }

  /**
   * on Enter press or click on search icon
   * 1. pretty print search JSON block on screen
   * 2. set state to default (original values + closes filters overlay)
   */
  onSearchSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation(); // for performance..no need for event to bubble up
    
    const {filtersVisible, ...toPrint} = this.state;
    this.props.updateJson(toPrint);
    this.setState({ ...defaultState });
  }

  render() {

    // don't want the search input width to keep jumping around when filters overlay open
    const searchInputWidth = this.state.filtersVisible && this.state.searchText && this.state.searchText !== 'Search...' ? 'forceWidth' : '';
    const {searchText, filtersVisible, ...propsToPass} = this.state;
    return (
        <div className="header-search">
          <form onSubmit={this.onSearchSubmit}> 
            <input className={searchInputWidth} type="text" value={this.state.searchText} onChange={this.handleSearchBoxChange} onClick={this.onSearchBoxClick}/>
            <button><i className="fa fa-search"></i></button>

            { 
              this.state.filtersVisible && 
              <Filters  {...propsToPass} handleChangeFor={this.handleChangeFor}/> 
            }

          </form>
        </div>
    );
  }
}

export default Search;
