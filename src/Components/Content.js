import React, { Component } from 'react';

class Content extends Component {
  render() {
    return (
      <div className="App-Content">
        { this.props.json ? <pre>{this.props.json}</pre> :  'Prints JSON block here'}
      </div>
    );
  }
}

export default Content;
