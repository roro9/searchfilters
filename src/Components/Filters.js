import React, { Component } from 'react';


class Filter extends Component {
    render() {
        return (
            <div className="filter">
                <div className="filter-label">{this.props.label}</div>
                <div className="filter-ctrls">
                    {this.props.ctrls}
                </div>
        </div>
        );
    }
}

class RadioButtons extends Component {
    render() {
        return(
            <React.Fragment>
                {
                    this.props.inputs.map(input => {
                        return (
                            <div className="inputBtn" key={input.text}>
                                {/* onChange on input is dummy...otherwise checked doesnt work...onChange is present on parentDiv (event delegation) */}
                                <input type="radio" name={this.props.name} data-field={input.text} checked={ this.props.filter_selected === input.text} onChange={(e)=>{ }} />
                                {input.text} 
                                { input.fa && <i className={input.fa}></i> }
                            </div>
                        );
                    })
                }
            </React.Fragment>
        );
    }
}

class Filters extends Component {
  render() {
    return (
        
    <div className="search-filters">
        
        {/* Filter -> Days */}
        <Filter label={`Listed in the last ${this.props.filter_days} days`}
                ctrls={
                    <input type="range" min="1" max="90" value={this.props.filter_days} step="1" onChange={(e) => this.props.handleChangeFor('filter_days', e.target.value)}/>
                } 
        />
        

        {/* Filter -> Sort By */}
        <Filter label={`Sort by ${this.props.filter_sortBy}`}
                ctrls={
                    <div className="buttonList" onChange={(e) => this.props.handleChangeFor('filter_sortBy', e.target.dataset.field)}>
                        <RadioButtons name="sort" filter_selected={this.props.filter_sortBy} inputs={[
                            { text: "Distance"                            },
                            { text: "Date"                                },
                            { text: "Price up",   fa: "fa fa-sort-up"     },
                            { text: "Price down", fa:"fa fa-sort-down"    }
                        ]}/>
                    </div>
                } 
        />
        

        {/* Filter -> Radius */}
        <Filter label={`Radius: ${this.props.filter_radius} kms`}
                ctrls={
                    <input type="range" min="0" max="500" value={this.props.filter_radius} step="5" onChange={(e) => this.props.handleChangeFor('filter_radius', e.target.value)}/>
                } 
        />
        

        {/* Filter -> Category */}
        <Filter label={`Category: ${this.props.filter_category}`}
                ctrls={
                    <div className="buttonList" onChange={(e) => this.props.handleChangeFor('filter_category', e.target.dataset.field)}>
                        <RadioButtons name="category" filter_selected={this.props.filter_category} inputs={[
                            { text: "All"          },
                            { text: "Fashion"      },
                            { text: "Electronics"  },
                            { text: "Sports"       },
                            { text: "Home"         },
                            { text: "Books"        },
                            { text: "Cars"         },
                            { text: "Other"        }
                        ]}/>
                    </div>
                } 
        />
        

        {/* Filter -> Price Range */}
        <Filter label={`Price Range($): ${this.props.filter_min_price} - ${this.props.filter_max_price}`}
                ctrls={
                    <div className="buttonList priceRng">
                        <div className="inputBtn">Min<input type="number" min="1" max="50000" name="price" value={this.props.filter_min_price} onChange={(e) => this.props.handleChangeFor('filter_min_price', e.target.value)}/></div>
                        <div className="inputBtn">Max<input type="number" min="1" max="50000" name="price" value={this.props.filter_max_price} onChange={(e) => this.props.handleChangeFor('filter_max_price', e.target.value)}/></div>
                    </div>
                } 
        />
        
    </div>
          
    );
  }
}

export default Filters;
