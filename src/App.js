import './App.css';
import React, { Component } from 'react';
import Header from './Components/Header.js';
import Content from './Components/Content.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: ''
    }
  }

  updateJson = (data) => {
    console.log('Search JSON data:');
    console.log(data);
    this.setState({json: data.searchText && data.searchText!=='Search...' ? JSON.stringify(data, undefined, 2) : '' });
  }

  render() {
    return (
      <div className="App">
        <Header updateJson={this.updateJson}/>
        <Content json={this.state.json}/>
      </div>
    );
  }
}

export default App;
