This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

UI for Shpock search bar with filters overlay. 

To run:

```sh
git clone https://roro9@bitbucket.org/roro9/searchfilters.git
npm install
npm start
```

![](https://bytebucket.org/roro9/searchfilters/raw/a3d2cc22b81bc258e72f718f4e02bfc48ab8c0e3/shpock.gif)